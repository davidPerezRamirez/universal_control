#include "IRremoteConfig.h"

const int RECV_PIN = D4; 
IRrecv irrecv(RECV_PIN);

const int SEND_PIN = 4;
IRsend irsend(SEND_PIN);

decode_results codigo;


void IRremoteConfig::sendNEC(const String& strCode) {
  char code[15];
  strCode.toCharArray(code, 15); 
  long codeIR = strtol(code, NULL, 0);
  
  irsend.sendNEC(codeIR, 32);
}

void IRremoteConfig::sendRAW(uint16_t codeRaw[], uint8_t sizeArray) {
  irsend.sendRaw(codeRaw, sizeArray, 38);
}

int IRremoteConfig::changeModeEdition(const String& status) {
  int isModeEdition = 0;
	
  if (status == "ACTIVE") {
    irrecv.enableIRIn(); //Inicialize Pin para recepcion de IR
    isModeEdition = 1;
  } else if (status == "DESACTIVE") {
    isModeEdition = 0;
    irrecv.disableIRIn();
  };
  
  return isModeEdition;
}

void IRremoteConfig::beginSend() {
	irsend.begin(); //Inicializa Pin de envio de IR
}

void IRremoteConfig::enabledRecive() {
	irrecv.enableIRIn();
    irrecv.resume();
}

void IRremoteConfig::decode(String& decodedCode) {

	if (irrecv.decode(&codigo)) {
		decodedCode = "";

		decodedCode += F("{");
		decodedCode += F("\n");
		// Hexa Code
		decodedCode += F("\"hexa_code\":\"");
		decodedCode += resultToHexidecimal(&codigo);;
		decodedCode += F("\",");

		//Raw Code
		decodedCode += F("\n");
		decodedCode += F("\"raw_code\": [");   // array name
		for (uint16_t i = 1; i < (&codigo)->rawlen; i++) {
		uint32_t usecs;
		for (usecs = (&codigo)->rawbuf[i] * kRawTick; usecs > UINT16_MAX;
			 usecs -= UINT16_MAX) {
			decodedCode += uint64ToString(UINT16_MAX);
			if (i % 2)
			  decodedCode += F(", 0,  ");
			else
			  decodedCode += F(",  0, ");
		  }
		  decodedCode += uint64ToString(usecs, 10);
		  if (i < (&codigo)->rawlen - 1)
			decodedCode += kCommaSpaceStr;            // ',' not needed on the last one
		  if (i % 2 == 0) decodedCode += ' ';  // Extra if it was even.
		}
		decodedCode += F("]");
		decodedCode += F("\n");
		decodedCode += F("}");
		
		Serial.println(decodedCode);
		Serial.println();
		
		delay(200);
		IRConfig.enabledRecive();
	}
}

IRremoteConfig IRConfig;