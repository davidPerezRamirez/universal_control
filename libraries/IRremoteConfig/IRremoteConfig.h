#ifndef IR_REMOTE_CONFIG_H
#define IR_REMOTE_CONFIG_H

#include <IRremoteESP8266.h>
#include <IRutils.h>
#include <IRtext.h>
#include <IRsend.h>
#include <IRrecv.h>

class IRremoteConfig {
	
	public:
	
		virtual void sendNEC(const String& code);
		
		virtual void sendRAW(uint16_t codeRaw[], uint8_t sizeArray);
		
		virtual int changeModeEdition(const String& status);
		
		virtual void beginSend();
		
		virtual void enabledRecive();
		
		virtual void decode(String& decodedCode);
};

extern IRremoteConfig IRConfig;

#endif // IR_REMOTE_CONFIG_H