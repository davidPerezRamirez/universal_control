#ifndef DATA_BASE_FIREBASE_CONFIG_H
#define DATA_BASE_FIREBASE_CONFIG_H

#include <FirebaseArduino.h>
#include <ArduinoJson.h>

class DataBaseFirebaseConfig {
	
	public:
	
		virtual void connect();
	
		virtual String getCodeInCloud(const String& key);
		
		virtual void deleteCode(const String& key);
};

extern DataBaseFirebaseConfig DataBase;

#endif // DATA_BASE_FIREBASE_CONFIG_H
