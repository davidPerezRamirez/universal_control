#include <IRrecv.h>
#include <IRsend.h>

#include <IRremoteESP8266.h>
#include <IRtext.h>
#include <IRutils.h>
#include <Arduino.h>

#define btn_on_music "0xB24D5FA0"
#define btn_on_tv "0x20DF10EF"
#define btn_vol_down "0x20DFC03F"

int RECV_PIN = D4; 
IRrecv irrecv(RECV_PIN);
decode_results codigo;
IRsend irsend(4);

void setup()
{
  Serial.begin(115200);
  irrecv.enableIRIn(); // Empezamos la recepción  por IR
  irsend.begin();
}

void loop() {
  if (irrecv.decode(&codigo)) {
    Serial.println(resultToSourceCode(&codigo));

    if (codigo.value == 0xDEE522C1 || codigo.value == 0xFD08F7) {//btn 1 control NEC
      Serial.println("Enciende musica:");
      long code = strtol(btn_on_music, NULL, 0);

      for (int i=0; i < 5; i++) {
        irsend.sendNEC(code, 32);
      }
      Serial.println("################# Envio #####################");
      Serial.println(code);
      Serial.println("################# Termino Envio #####################");
    }
    if (codigo.value == 0xFD8877 || codigo.value == 0x6F5974BD) { //btn 2 control NEC
      Serial.println("Enciende televisor:");
      for (int i=0; i < 5; i++) {
        long code = strtol(btn_vol_down, NULL, 0);
        
        irsend.sendNEC(code, 32);
      }
    }
    if (codigo.value == 0x6FD48B7 || codigo.value == 0xFD48B7) { //btn 3 control NEC
      Serial.println("Enciende AC:");
      int khz = 38; // 38kHz carrier frequency for the NEC protocol
      uint16_t irSignalAC_ON[] = {4434, 4372,  566, 1618,  566, 536,  560, 1624,  566, 1620,  564, 532,  564, 534,  562, 1620,  564, 536,  562, 536,  564, 1624,  564, 532,  564, 532,  508, 1676,  566, 1620,  566, 532,  562, 1626,  566, 1626,  516, 584,  564, 1626,  564, 1620,  566, 1622,  508, 1676,  510, 1678,  564, 1622,  510, 588,  568, 1622,  564, 532,  508, 590,  564, 532,  564, 530,  566, 532,  562, 536,  564, 536,  564, 530,  506, 590,  564, 532,  564, 534,  566, 530,  564, 534,  562, 534,  564, 1622,  566, 1620,  568, 1618,  568, 1618,  568, 1620,  564, 1620,  566, 1620,  566, 1620,  508, 5272,  4434, 4376,  508, 1676,  566, 532,  564, 1624,  508, 1678,  566, 532,  506, 590,  506, 1678,  564, 532,  566, 534,  562, 1626,  566, 532,  566, 530,  564, 1620,  566, 1618,  566, 534,  564, 1628,  508, 1682,  566, 532,  564, 1626,  508, 1678,  566, 1618,  566, 1620,  568, 1618,  564, 1622,  566, 534,  564, 1620,  566, 532,  566, 532,  562, 534,  564, 532,  566, 530,  566, 534,  564, 536,  562, 532,  564, 534,  564, 532,  564, 532,  562, 534,  504, 590,  506, 592,  564, 1624,  512, 1674,  566, 1618,  568, 1620,  508, 1678,  508, 1678,  510, 1674,  568, 1622,  508};

      irsend.sendRaw(irSignalAC_ON, sizeof(irSignalAC_ON) / sizeof(irSignalAC_ON[0]), khz); //Note the approach used to automatically calculate the size of the array.
      delay(2000);
    }
    Serial.print("SENDING DATA: ");
    delay(200);
    irrecv.enableIRIn();
    irrecv.resume(); // Receive the next value;
  }
  
}
