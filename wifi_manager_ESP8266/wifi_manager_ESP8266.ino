#include <DataBaseFirebaseConfig.h>

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

#include <DNSServer.h>
#include <WiFiManager.h>
#include <IRremoteConfig.h>

#define led 13

ESP8266WebServer server(80);

//default custom static IP
const char static_ip[16] = "192.168.0.10";
const char static_gw[16] = "192.168.0.1";
const char static_sn[16] = "255.255.255.0";
String wifi_status = "";
int isModeEdition = 0;
String decodedCode = "";
String response;

String userId = "";

void handleRoot() {
  digitalWrite(led, 1);
  server.send(200, "text/plain", "hello from esp8266!");
  digitalWrite(led, 0);
}

void connectionStatus()
{
  response = "";
  digitalWrite(led, 1);
  response += "{";
  response += "\"status\":";
  if (WiFi.status() != WL_CONNECTED) {
    wifi_status = "El dispositivo no está conectado";
  } else {
    wifi_status = "El dispositivo esta conectado a la red:";
    wifi_status += WiFi.SSID();
    wifi_status += "\n";
    wifi_status += "Con la Ip:";
    wifi_status += WiFi.localIP();

    response += "\"CONNECTED\",";
  }
  String isLogged = "";
  if (userId.length() > 0) { 
    isLogged = "true";
  } else {
    isLogged = "false";
  }
  response += "\"is_logged\":\"";
  response += isLogged;
  response += "\"}";
  
  Serial.println(wifi_status);
  server.send(200, "text/plain", response);
  digitalWrite(led, 0);
}

void sendNec() { 
  digitalWrite(led, 1);
  String strCode = server.arg("code");
  IRConfig.sendNEC(strCode);
  
  Serial.println("############Inicia envio#####################");
  Serial.println( server.arg("code"));
  Serial.println("############Termina envio#####################");
  
  delay(200);
  server.send(200, "text/json", "{\"status\": \"SUCCESS\"}");
  digitalWrite(led, 0);
}

void sendRawCode() {
  digitalWrite(led, 1);
  
  if (server.arg("code").length() > 0) {
    sendArrayCode(server.arg("code"));
    delay(200);
    server.send(200, "text/json", "{\"status\": \"SUCCESS\"}");
    Serial.println("############ Raw Enviado #####################");
  } else {
    server.send(200, "text/json", "{\"status\": \"FAILURE\"}");
    Serial.println("############ Raw FALLIDO #####################");
  }
  digitalWrite(led, 0);
}

 void sendArrayCode(String strCode) {
  uint16_t codeRaw[199];
  char code[strCode.length() + 1];
  strCode.toCharArray(code, sizeof(code));
 
  char delimiters[] = ",";
  char *ptr;
  int index = 0;

  ptr = strtok(code, delimiters);
  
  Serial.println("############Inicia envio#####################");
  Serial.print("[");
  while (ptr != NULL) {
    codeRaw[index] = atoi(ptr);
    Serial.print(codeRaw[index]); Serial.print(",");
    index++;
    ptr = strtok(NULL, delimiters);
  }
  Serial.print("]");
  Serial.println();
  Serial.println("############Termina envio#####################");
  delete[] ptr;

  IRConfig.sendRAW(codeRaw, sizeof(codeRaw) / sizeof(codeRaw[0]));
}

void saveUserId() {
  digitalWrite(led, 1);
  String body = server.arg("plain");
  StaticJsonBuffer<200> jsonBuffer;

  JsonObject& root = jsonBuffer.parseObject(body);
  if (!root.success()) {
    String strError = "{";
    strError += ("\"error\": \"fail to parse json\",");
    strError += "\"status\":\"FAIL\"";
    strError += "}";
    server.send(200, "text/json", strError);
  }
  userId = root["user_id"].asString();

  String response = "";
  response += "{";
  response += "\"user_id\":\"" + String(userId) +"\",";
  response += "\"status\":\"SUCCESS\"";
  response += "}";
  server.send(200, "text/json", response);
  digitalWrite(led, 0);
}

void changeModeEdition() {
  digitalWrite(led, 1);
  String strCode = server.arg("status");
  isModeEdition = IRConfig.changeModeEdition(strCode);
  response = "";

  if (strCode == "DESACTIVE") {
   decodedCode = "";
  }
  
  Serial.println("############Modo Edicion#####################");
  Serial.println( "status edition mode:" + server.arg("status"));
  Serial.println("#############################################");
  
  delay(200);
  
  response += F("{\"status\": \"SUCCESS\", \"is_mode_edition\":");
  response += String(isModeEdition);
  response += F("}");
  server.send(200, "text/json", response);
  digitalWrite(led, 0);
}

void getDecodedCode() {
  digitalWrite(led, 1);
  server.send(200, "text/json", decodedCode);
  digitalWrite(led, 0);
}

void getUserId() {
  digitalWrite(led, 1);
  server.send(200, "text/json", userId);
  digitalWrite(led, 0);
}

void handleNotFound(){
  digitalWrite(led, 1);
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
  digitalWrite(led, 0);
}

void decode() {
  if (isModeEdition){
    IRConfig.decode(decodedCode);
  }
}

void sendNECCodeByFirebase() {
  String strCode = DataBase.getCodeInCloud(userId);
  
  if (strCode.length() != 0) {
    if (isRawCode(strCode)) {
      sendArrayCode(strCode);
    } else {
      IRConfig.sendNEC(strCode);
    }

    Serial.println("############Inicia envio#####################");
    Serial.println( strCode);
    Serial.println("############Termina envio#####################");
    server.send(200, "text/json", strCode);
    
    DataBase.deleteCode(userId);
  }
}

boolean isRawCode(String& code) {
  return code.indexOf(",") > 0;
}

void setup(void){
  WiFiManager wifiManager;

  //Limpia todas las conexiones wifi guardas en el modulo esp8266
  //wifiManager.resetSettings(); 

  IPAddress _ip,_gw,_sn;
  _ip.fromString(static_ip);
  _gw.fromString(static_gw);
  _sn.fromString(static_sn);
  
  wifiManager.setSTAStaticIPConfig(_ip, _gw, _sn);
  
  pinMode(led, OUTPUT);
  digitalWrite(led, 0);
  Serial.begin(115200);
  wifiManager.autoConnect("AutoConnectAP");
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(WiFi.SSID());
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }
  WiFi.setAutoReconnect(true);
  WiFi.persistent(true);

  server.on("/", handleRoot);
  server.on("/checkStatusConnection", connectionStatus);
  server.on("/sendNEC", HTTP_GET, sendNec);
  server.on("/sendRaw", HTTP_POST, sendRawCode);
  server.on("/getDecodedCode", HTTP_GET, getDecodedCode);
  server.on("/changeModeEdition", HTTP_GET, changeModeEdition);
  server.on("/saveUserId", HTTP_POST, saveUserId);
  server.on("/getUserId", HTTP_GET, getUserId);
  server.on("/sendByFirebase", HTTP_GET, sendNECCodeByFirebase);
  server.onNotFound(handleNotFound);
  server.begin();
  
  IRConfig.beginSend();

  DataBase.connect();
  Serial.println("HTTP server started");
}

void loop(void){
  decode();
  sendNECCodeByFirebase();
  
  server.handleClient();
}
